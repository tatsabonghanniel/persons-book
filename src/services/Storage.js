import { firebase_app } from "./Firebase";

export default class StorageServices {
    uploadImage(image) {
        const date = new Date().getTime()
        let file = image;
        var storage = firebase_app.storage();
        var storageRef = storage.ref();
        return storageRef.child('Persons/' + file.name + '-' + date).put(file);
    }
}