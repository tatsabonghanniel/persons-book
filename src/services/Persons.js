import { firebase_app } from "./Firebase";

export default class PersonsServices {
    create(person) {
        const date = new Date();
        return new Promise(async (resolve, reject) => {
            await firebase_app.firestore()
                .collection('Persons')
                .add({ ...person, created_at: date.getTime() })
                .then(resolve)
                .catch(reject)
        })
    }

    update(person, id) {
        const date = new Date();
        return new Promise(async (resolve, reject) => {
            await firebase_app.firestore()
                .collection('Persons')
                .doc(id)
                .update({ ...person, last_update: date.getTime() })
                .then(resolve)
                .catch(reject)
        })
    }

    delete(id) {
        return new Promise(async (resolve, reject) => {
            await firebase_app.firestore()
                .collection('Persons')
                .doc(id)
                .delete()
                .then(resolve)
                .catch(reject)
        })
    }
}