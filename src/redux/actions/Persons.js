import { firebase_app } from '../../services/Firebase';
import * as ActionsType from '../constants/Persons';

export const getPersonsAction = () => async dispatch => {

    dispatch({ type: ActionsType.GET_PERSONS_REQUEST });

    await firebase_app.firestore()
        .collection('Persons')
        .get()
        .then((querySnapshot) => {
            let result = [];

            querySnapshot.forEach(documentSnapshot => {
                result.push({ ...documentSnapshot.data(), id: documentSnapshot.id });
            });

            dispatch({
                type: ActionsType.GET_PERSONS_SUCCESS,
                payload: { persons: result },
            });
        })
        .catch((e) => {
            console.error(e);
            dispatch({
                type: ActionsType.GET_PERSONS_FAIL,
                payload: { error: "Erreur lors de la recuperation de la liste de personnes" },
            });
        });

}
