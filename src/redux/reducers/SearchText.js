export const searchTextReducer = (state = "", action) => {
    switch (action.type) {
        case "SET_SEARCH_TEXT":
            return {
                loading: true,
                searchText: action.payload.searchText,
            }
        default:
            return state;
    }
}
