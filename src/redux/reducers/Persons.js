import * as ActionsType from "../constants/Persons";

export const PersonsReducer = (state = [], action) => {
    switch (action.type) {
        case ActionsType.GET_PERSONS_REQUEST:
            return {
                loading: true,
                persons: [],
            }
        case ActionsType.GET_PERSONS_SUCCESS:
            return {
                loading: false,
                persons: action.payload.persons,
            }
        case ActionsType.GET_PERSONS_FAIL:
            return {
                loading: false,
                error: action.error
            }
        default:
            return state;
    }
}
