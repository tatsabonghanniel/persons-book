import { applyMiddleware, combineReducers, createStore } from "redux";
import * as PersonsReducers from "./reducers/Persons";
import * as SearchTextReducers from "./reducers/SearchText";

import thunk from 'redux-thunk';

export default createStore(
    combineReducers({
        persons: PersonsReducers.PersonsReducer,
        searchText: SearchTextReducers.searchTextReducer
    }),
    applyMiddleware(thunk)
);