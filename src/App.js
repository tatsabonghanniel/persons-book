import React from 'react'

import { Provider } from 'react-redux';
import store from './redux/store';

import CssBaseline from '@material-ui/core/CssBaseline';

import AppTopBar from './components/AppTopBar';
import ListPersons from './components/ListPersons';

const App = () => {

  return (
    <Provider store={store}>
      <CssBaseline />
      <AppTopBar />
      <ListPersons />
    </Provider>
  );
}

export default App;
