import React, { useEffect, useState } from 'react'

import { useDispatch, useSelector } from 'react-redux';
import { getPersonsAction } from '../redux/actions/Persons';

import { Button, Dialog, Avatar, DialogActions, DialogContent, DialogContentText, DialogTitle, Snackbar } from '@material-ui/core';
import { DataGrid } from '@material-ui/data-grid';

import EditIcon from '@material-ui/icons/Edit'
import DeleteIcon from '@material-ui/icons/Delete'

import { Alert } from '@material-ui/lab';

import PersonForm from './Forms/PersonForm';
import LoadingModal from './Forms/LoadingModal';

import { PersonsProvider } from '../providers/Persons';
import { StorageProvider } from '../providers/Storage';

const ListPersons = () => {

    const dispatch = useDispatch();

    const getPersons = useSelector(state => state.persons);
    const { loading = true, persons = [] } = getPersons;

    const getSearchText = useSelector(state => state.searchText);
    const { searchText = '' } = getSearchText;

    const [updating, setUpdating] = useState(false);
    const [deleting, setDeleting] = useState(false);
    const [selected, setSelected] = useState({});

    const [personsToDisplay, setPersonsToDisplay] = useState([]);

    const [successMessage, setSuccessMessage] = useState({ visible: false, message: '' });
    const [processing, setProcessing] = useState(false);


    useEffect(() => {

        dispatch(getPersonsAction());

        return () => {

        }
    }, [dispatch]);

    const columns = [
        { field: 'index', headerName: 'ID', width: 60 },
        {
            width: 120,
            field: 'avatar',
            headerName: 'Image',
            sortable: false,
            filterable: false,
            renderCell: (params) => {
                return (
                    <Avatar alt={params.row.nom} title={params.row.nom} src={params.row.avatar} />
                )
            }
        },
        {
            field: 'nom',
            headerName: 'Nom',
            width: 160,
            editable: false,
        },
        {
            field: 'prenom',
            headerName: 'Prénom',
            width: 160,
            editable: false,
        },
        {
            field: 'email',
            headerName: 'Email',
            width: 210,
            editable: false,
        },
        {
            field: 'telephone',
            headerName: 'Téléphone',
            width: 150,
            editable: false,
        },
        {
            field: 'nomComplet',
            headerName: 'Nom complet',
            sortable: false,
            filterable: false,
            width: 160,
            valueGetter: (params) =>
                `${params.getValue(params.id, 'nom') || ''} ${params.getValue(params.id, 'prenom') || ''
                }`,
        },
    ];

    const handleUpdatePerson = async (infos) => {
        setProcessing(true);

        let avatar = infos.avatar;

        if (infos.avatar && infos.avatar.name) {
            await StorageProvider.uploadImage(infos.avatar)
                .then(async (snapshot) => {
                    avatar = await snapshot.ref.getDownloadURL();
                })
                .catch((e) => {
                    console.log(e);
                })
        }

        await PersonsProvider.update({ ...infos, avatar: avatar }, selected.id);

        setSuccessMessage({ visible: 'true', message: 'Personne mise a jour avec succès !' });

        dispatch(getPersonsAction());

        setUpdating(false);
        setProcessing(false);
    }

    const handleDeletePerson = async () => {
        setProcessing(true);
        await PersonsProvider.delete(selected.id)
            .then(() => {
                setSuccessMessage({ visible: 'true', message: 'Personne effacée avec succès !' });
                dispatch(getPersonsAction());
            })
            .catch((e) => {
                console.log(e);
            });

        setProcessing(false);
    }

    if (persons.length !== personsToDisplay.length && !searchText) {
        setPersonsToDisplay(persons);
    }


    return (
        <div style={{ height: 400 }}>

            <Dialog
                open={deleting}
                onClose={() => { setDeleting(false) }}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">{"Supprimer une personne ?"}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        En acceptant ceci vous effacerez <b>{selected.nom + " " + selected.prenom}</b>.
                        Confirmez-vous ???
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => { setDeleting(false) }} color="primary">
                        Annuler
                    </Button>
                    <Button onClick={() => { setDeleting(false); handleDeletePerson(); }} color="primary" autoFocus>
                        Accepter
                    </Button>
                </DialogActions>
            </Dialog>


            <PersonForm
                open={updating}
                handleClose={() => { setUpdating(false) }}
                handleSubmit={handleUpdatePerson}
                defaultDatas={selected}
            />

            <LoadingModal open={loading || processing} />

            <Snackbar open={successMessage.visible} autoHideDuration={4000} onClose={() => { setSuccessMessage({ visible: false }) }}>
                <Alert variant='filled' elevation={6} onClose={() => { setSuccessMessage({ visible: false }) }} severity="success">
                    {successMessage.message}
                </Alert>
            </Snackbar>

            <DataGrid
                rows={personsToDisplay.filter((person) => (person.nom + " " + person.prenom + " " + person.telephone + ' ' + person.email).toLowerCase().indexOf(searchText.toLowerCase()) !== -1).map((person, index) => ({ ...person, index: (index + 1) }))}
                columns={[...columns, {
                    width: 200,
                    field: 'actions',
                    headerName: 'Actions',
                    sortable: false,
                    filterable: false,
                    renderCell: (params) => {
                        return (
                            <div>
                                <Button onClick={() => { setSelected(params.row); setUpdating(true) }} variant="contained" color="primary">
                                    <EditIcon />
                                </Button>
                                <Button onClick={() => { setSelected(params.row); setDeleting(true) }} variant="contained" color="secondary">
                                    <DeleteIcon />
                                </Button>
                            </div>
                        )
                    }
                }]}
                pageSize={5}
                localeText={{
                    columnMenuSortAsc: 'Classer Croissant',
                    columnMenuSortDesc: 'Classer Déroissant',
                    columnMenuFilter: 'Filtrer',
                    columnMenuHideColumn: 'Cacher la colone',
                    columnMenuShowColumns: 'Afficher des colones',
                    columnMenuUnsort: 'Annuler le classement',

                    filterPanelAddFilter: 'Ajouter un filtre',
                    filterPanelColumns: 'Colones',
                    filterPanelInputLabel: 'Valeur à chercher',
                    filterPanelInputPlaceholder: 'Valeur...',

                    filterPanelOperators: 'Opérateurs',
                    filterOperatorContains: 'Contient',
                    filterOperatorEndsWith: 'Se termine par',
                    filterOperatorStartsWith: 'Commence par',
                    filterOperatorEquals: 'Est égal à',

                    columnsPanelHideAllButton: 'Tout cacher',
                    columnsPanelShowAllButton: 'Tout afficher',
                    columnsPanelTextFieldLabel: 'Chercher une colone',
                    columnsPanelTextFieldPlaceholder: 'Titre de la colone',

                    noRowsLabel: 'Aucune donnés',
                    noResultsOverlayLabel: 'Aucun résultat trouvé',

                    footerRowSelected: (count) => <span>{count} ligne{count > 1 ? 's sélectionnées' : ' sélectionnée'}</span>,

                }}
            />

        </div>
    )
}

export default ListPersons
