import React, { useState } from 'react';

import { Snackbar, AppBar, Toolbar, IconButton, Typography, InputBase } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import SearchIcon from '@material-ui/icons/Search';
import AddIcon from '@material-ui/icons/Add';

import PersonForm from './Forms/PersonForm';
import LoadingModal from './Forms/LoadingModal';

import useStyles from './styles'

import { useDispatch } from 'react-redux';
import { getPersonsAction } from '../redux/actions/Persons';
import { StorageProvider } from '../providers/Storage';
import { PersonsProvider } from '../providers/Persons';

const AppTopBar = () => {

    const classes = useStyles();

    const [search, setSearch] = useState('');

    const [modalVisible, setModalVisible] = useState(false);
    const [loading, setLoading] = useState(false);

    const [successVisible, setSuccessVisible] = useState(false);

    const dispatch = useDispatch();

    const openModal = () => {
        setModalVisible(true);
    }

    const change = e => {
        setSearch(e.target.value);
        dispatch({
            type: "SET_SEARCH_TEXT",
            payload: { searchText: e.target.value },
        });
    };

    const handleAddPerson = async (infos) => {
        setLoading(true);

        let avatar = null;

        if (infos.avatar) {
            await StorageProvider.uploadImage(infos.avatar)
                .then(async (snapshot) => {
                    avatar = await snapshot.ref.getDownloadURL();
                })
                .catch((e) => {
                    console.log(e);
                })
        }

        await PersonsProvider.create({ ...infos, avatar: avatar });

        setSuccessVisible(true);
        setModalVisible(false);
        setLoading(false);
        dispatch(getPersonsAction());
    }

    return (
        <AppBar position="static">

            <PersonForm
                open={modalVisible}
                handleClose={() => { setModalVisible(false) }}
                handleSubmit={handleAddPerson}
            />

            <LoadingModal open={loading} />

            <Snackbar open={successVisible} autoHideDuration={4000} onClose={() => { setSuccessVisible(false) }}>
                <Alert variant='filled' elevation={6} onClose={() => { setSuccessVisible(false) }} severity="success">
                    Personne enregistrée avec succès !
                </Alert>
            </Snackbar>

            <Toolbar>
                <Typography className={classes.title} variant="h6" noWrap>
                    Gestion de personnes
                </Typography>
                <div className={classes.grow} />
                <IconButton onClick={openModal} color="inherit">
                    <AddIcon />
                </IconButton>

                <div className={classes.search}>
                    <div className={classes.searchIcon}>
                        <SearchIcon />
                    </div>
                    <InputBase
                        placeholder="Recherche…"
                        classes={{
                            root: classes.inputRoot,
                            input: classes.inputInput,
                        }}
                        value={search}
                        onChange={change}
                    />
                </div>
            </Toolbar>
        </AppBar>
    )
}

export default AppTopBar;