
import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(theme => ({
    formModal: {
        display: 'flex',
        width: '100vw',
        height: '100vh',
        justifyContent: 'center',
        alignItems: 'center'
    },
    formContent: {
        padding: '20px'
    },
    closeButton: {
        display: 'flex',
        justifyContent: 'flex-end'
    },
    formHeader: {
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'column'
    },
    avatar: {
        width: '150px', 
        height: '150px'
    },
    cameraButton: {
        background: '#3f51b5',
        position: 'absolute',
        marginTop: '100px',
        marginLeft: '100px',
        justifyContent: 'center',
        alignItems: 'center',
        padding: '10px 10px 5px 10px',
        borderRadius: '50%',
        cursor: 'pointer'
    },
    formControl: {
        display: 'flex',
        width: '100%',
        marginTop: '15px'
    }
}));
