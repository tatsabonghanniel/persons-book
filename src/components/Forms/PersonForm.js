import React, { useState, useEffect } from 'react'

import { Button, Container, Modal, Paper, Box, Typography, TextField, Avatar, InputLabel } from '@material-ui/core'
import CameraIcon from '@material-ui/icons/CameraAlt'
import useStyles from './styles'

const PersonForm = ({ open, handleClose, handleSubmit, defaultDatas = null }) => {

    const classes = useStyles();

    const [values, setValues] = useState({ nom: '', prenom: '', email: '', telephone: '', avatar: null });

    const [displayImage, setDisplayImage] = useState('');

    useEffect(() => {

        if (defaultDatas) {
            setValues(defaultDatas);
            if (defaultDatas.avatar) {
                setDisplayImage(defaultDatas.avatar);
            } else {
                setDisplayImage('');
            }
        }

        return () => {

        }
    }, [defaultDatas])

    const handleChange = (e) => {
        let vals = values;
        vals[e.target.name] = e.target.value;

        setValues(vals);
    }

    const handleChangeImage = (e) => {
        if (e.target.files[0]) {
            console.log(e.target.files[0]);
            setDisplayImage(URL.createObjectURL(e.target.files[0]));
            setValues(vals => ({ ...vals, avatar: e.target.files[0] }));
        } else {
            setDisplayImage('');
            setValues(vals => ({ ...vals, avatar: null }));
        }
    }

    return (
        <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
        >
            <Container classes={{ root: classes.formModal }}>
                <Paper classes={{ root: classes.formContent }}>

                    <div className={classes.closeButton}>
                        <Button onClick={handleClose}>X</Button>
                    </div>

                    <form onSubmit={(e) => { e.preventDefault(); handleSubmit(values); }}>
                        <Box className={classes.formHeader}>

                            <Avatar className={classes.avatar} alt="" src={displayImage} />

                            <InputLabel className={classes.cameraButton} htmlFor='file'>
                                <CameraIcon style={{ color: '#FFF' }} />
                            </InputLabel>


                            <input style={{ display: 'none' }} accept="image/*" id='file' type='file' onChange={handleChangeImage} />

                            <Typography className={classes.title} variant="h5" noWrap>
                                {defaultDatas ? "Modification d'une personne" : "Ajout d'une personne"}
                            </Typography>

                        </Box>


                        <div className={classes.formControl}>
                            <TextField
                                defaultValue={values.nom}
                                onChange={handleChange}
                                required label="Nom" name='nom' variant="outlined"
                            />
                            <TextField
                                defaultValue={values.prenom}
                                onChange={handleChange}
                                required label="Prénom" name='prenom' variant="outlined"
                                style={{ marginLeft: '15px' }}
                            />
                        </div>

                        <TextField
                            className={classes.formControl}
                            defaultValue={values.email}
                            onChange={handleChange} required label="Email" name='email' variant="outlined"
                        />

                        <TextField
                            className={classes.formControl}
                            defaultValue={values.telephone}
                            onChange={handleChange}
                            required label="Numéro de téléphone" name='telephone' variant="outlined"
                        />

                        <Button className={classes.formControl} type='submit' variant='contained' color='primary'>Valider</Button>

                    </form>

                </Paper>
            </Container>
        </Modal>
    )
}

export default PersonForm
