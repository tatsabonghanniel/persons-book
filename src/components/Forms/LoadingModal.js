import React from 'react'

import { Container, Modal, Paper, CircularProgress } from '@material-ui/core'
import useStyles from './styles'

const LoadingModal = ({ open }) => {

    const classes = useStyles();

    return (
        <Modal
            open={open}
            onClose={() => { }}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
        >
            <Container classes={{ root: classes.formModal }}>
                <Paper classes={{ root: classes.formContent }}>
                    <div className={classes.formHeader}>
                        <CircularProgress />
                    </div>
                </Paper>
            </Container>
        </Modal>
    )
}

export default LoadingModal
